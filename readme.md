# How this task will be evaluated
# We expect your solution to the task to meet the following criteria:

* You have used transition properties
* If you had changed the HTML markup, you described why you did it
* All three effects (logo rotation and zooming, rotation of the Read More button, the Video button transformation) are applied by mousing over an element